package com.wxs.boot01.consts;

public class ActionConsts {

    public static final Integer COMMON_CONTENT_TYPE = 0;

    public static final Integer OWN_SCREEN_CONTENT_TYPE = 1;

    public static final Integer DASHANG_CONTENT_TYPE = 2;

    public static final Integer GIFT_CONTENT_TYPE = 3;

    public static final Integer RED_PACKAGE_TYPE = 4;

    public static final Integer EXPRESS_LOVE_TYPE = 5;

    public static final Integer SONG_CONTENT_TYPE = 7;

    public static final Integer SPELLSONG_CONTENT_TYPE = 8;

    public static final Integer SPELLSONGFINISH_CONTENT_TYPE = 9;
}
