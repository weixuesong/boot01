package com.wxs.boot01.mapper;


import com.wxs.boot01.entity.WeishangqiangContent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface WeishangqiangContentMapper {

    List<Map<String,Object>> queyContentInfo(@Param("weishangqiangId") Integer weishangqiangId) throws Exception;

    Integer insertSelective(WeishangqiangContent weishangqiangContent) throws Exception;

}
