package com.wxs.boot01.mapper;


import com.wxs.boot01.entity.WechatUser;
import com.wxs.boot01.entity.WeishangqiangContent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface WechatUserMapper {

    WechatUser selectByPrimaryKey(Integer id) throws Exception;

}
