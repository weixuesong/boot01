package com.wxs.boot01.util;

public class StringUtil {

	/**
	 * 微信消息缓存key
	 */
	public static final String WXIM_KEY = "wximkey";
	/**
	 * 酒吧消息缓存key
	 */
	public static final String WSQ_KEY = "wsqkey";
	/**
	 * 删除消息缓存key
	 */
	public static final String WSQ_DELETEKEY = "wsqdeletekey";
	/**
	 * 删除用户消息的用户ID缓存key
	 */
	public static final String WSQ_WECHATUSERIDKEY = "wsqwechatuseridkey";
	/**
	 * 删除消息类型缓存key
	 */
	public static final String WSQ_TYPEKEY = "wsqtypekey";
	/**
	 * 酒吧打赏礼物缓存key
	 */
	public static final String WSQ_DS_KEY = "dashangkey";
	/**
	 * 酒吧礼物缓存key
	 */
	public static final String WSQ_GIFT_KEY = "giftkey";
	/**
	 * 酒吧打赏tar缓存key
	 */
	public static final String WSQ_DSTAR_KEY = "dashangtarkey";
	/**
	 * 大屏消息缓存key
	 */
	public static final String DPIM_KEY = "dpimkey";
	/**
	 * 判断大屏消息开启状态缓存key
	 */
	public static final String CHECK_BIG = "checkbig";
	
	/**
	 * 酒吧用户缓存key
	 */
	public static final String WSQ_USER = "wsquser";
	/**
	 * 霸屏价格缓存key
	 */
	public static final String WSQ_BP = "ownscreenlist";
	/**
	 * 霸屏主题缓存key
	 */
	public static final String WSQ_THEME = "themelList";
	/**
	 * short缓存key
	 */
	public static final String WSQ_SHORT = "short";
	/**
	 * 酒吧与微上墙缓存key
	 */
	public static final String WSQ_BSWSQ = "bswsq";
	/**
	 * 赛马游戏人数缓存key
	 */
	public static final String WSQ_SMGAME = "wsqsmgame";
	/**
	 * 赛马游戏临时用户缓存key
	 */
	public static final String WSQ_SMGAMELK = "wsqsmgamelk";
	/**
	 * 赛马游戏临时用户缓存key
	 */
	public static final String WSQ_SMGAMEBIGSTATE = "wsqsmgamebigstate";
	/**
	 * 赛马游戏用户状态缓存key
	 */
	public static final String WSQ_SMGAMEUSERSTATE = "wsqsmgameuserstate";
	
	/**
	 * 赛马游戏比赛状态缓存key
	 */
	public static final String WSQ_SMGAMESTATE = "wsqsmgamestate";
	/**
	 * 赛马游戏比赛成绩缓存key       
	 */
	public static final String WSQ_SMGAMESCORE = "wsqsmgamescore";
	/**
	 * 赛马游戏比赛成绩缓存key       
	 */
	public static final String WSQ_ACTION = "wsqaction";
	
	/**
	 * 赛che游戏比赛成绩缓存key       
	 */
	public static final String WSQ_CSACTION = "wsqcsaction";
	
	/**
	 * 赛车游戏人数缓存key
	 */
	public static final String WSQ_RACEGAME = "wsqracegame";
	/**
	 * 赛车游戏临时用户缓存key
	 */
	public static final String WSQ_RACEGAMELK = "wsqracegamelk";
	/**
	 * 赛车游戏临时用户缓存key
	 */
	public static final String WSQ_RACEGAMEBIGSTATE = "wsqracegamebigstate";
	/**
	 * 赛车游戏用户状态缓存key
	 */
	public static final String WSQ_RACEGAMEUSERSTATE = "wsqracegameuserstate";
	
	/**
	 * 赛车游戏比赛状态缓存key
	 */
	public static final String WSQ_RACEGAMESTATE = "wsqracegamestate";
	/**
	 * 赛车游戏比赛成绩缓存key       
	 */
	public static final String WSQ_RACEGAMESCORE = "wsqracegamescore";
	
	/**
	 * 摇一摇游戏人数缓存key
	 */
	public static final String WSQ_SHAKEGAME = "wsqshakegame";
	/**
	 * 摇一摇游戏临时用户缓存key
	 */
	public static final String WSQ_SHAKEGAMELK = "wsqshakegamelk";
	/**
	 * 摇一摇游戏临时用户缓存key
	 */
	public static final String WSQ_SHAKEGAMEBIGSTATE = "wsqshakegamebigstate";
	/**
	 * 摇一摇游戏用户状态缓存key
	 */
	public static final String WSQ_SHAKEGAMEUSERSTATE = "wsqshakegameuserstate";
	
	/**
	 * 摇一摇游戏比赛状态缓存key
	 */
	public static final String WSQ_SHAKEGAMESTATE = "wsqshakegamestate";
	/**
	 * 摇一摇游戏比赛成绩缓存key       
	 */
	public static final String WSQ_SHAKEGAMESCORE = "wsqshakegamescore";
	/**
	 * 摇一摇游戏比赛成绩缓存key       
	 */
	public static final String WSQ_SHAKEACTION = "wsqshakeaction";

	/**
	 * 摇一摇游戏用户排名缓存
	 */
	public static final String WSQ_SHAKERANK= "wsqshakerank";
	
	/**
	 * 拔河游戏用户排名缓存
	 */
	public static final String WSQ_BAHERANK= "wsqbaherank";
	/**
	 * 拔河游戏人数缓存key
	 */
	public static final String WSQ_BHGAME = "wsqbhgame";
	/**
	 * 拔河游戏红队人数缓存key
	 */
	public static final String WSQ_BHHONGGAME = "wsqbhhonggame";
	/**
	 * 拔河游戏蓝队人数缓存key
	 */
	public static final String WSQ_BHLANGAME = "wsqbhlangame";
	/**
	 * 拔河游戏临时用户缓存key
	 */
	public static final String WSQ_BHGAMELK = "wsqbhgamelk";
	/**
	 * 拔河游戏临时用户缓存key
	 */
	public static final String WSQ_BHGAMEBIGSTATE = "wsqbhgamebigstate";
	/**
	 * 拔河游戏用户状态缓存key
	 */
	public static final String WSQ_BHGAMEUSERSTATE = "wsqbhgameuserstate";
	
	/**
	 * 拔河游戏比赛状态缓存key
	 */
	public static final String WSQ_BHGAMESTATE = "wsqbhgamestate";
	/**
	 * 拔河游戏比赛成绩缓存key       
	 */
	public static final String WSQ_BHGAMESCORE = "wsqbhgamescore";
	/**
	 * 拔河游戏红队比赛成绩缓存key       
	 */
	public static final String WSQ_BHREDGAMESCORE = "wsqbhredgamescore";
	/**
	 * 拔河游戏蓝队比赛成绩缓存key       
	 */
	public static final String WSQ_BHBLUEGAMESCORE = "wsqbhbluegamescore";
	/**
	 * 拔河游戏比赛成绩缓存key       
	 */
	public static final String WSQ_BHACTION = "wsqbhaction";
	
	/**
	 * 数钱游戏人数缓存key
	 */
	public static final String WSQ_SQGAME = "wsqsqgame";
	/**
	 * 数钱游戏临时用户缓存key
	 */
	public static final String WSQ_SQGAMELK = "wsqsqgamelk";
	/**
	 * 数钱游戏临时用户缓存key
	 */
	public static final String WSQ_SQGAMEBIGSTATE = "wsqsqgamebigstate";
	/**
	 * 数钱游戏用户状态缓存key
	 */
	public static final String WSQ_SQGAMEUSERSTATE = "wsqsqgameuserstate";
	
	/**
	 * 数钱游戏比赛状态缓存key
	 */
	public static final String WSQ_SQGAMESTATE = "wsqsqgamestate";
	/**
	 * 数钱游戏比赛成绩缓存key       
	 */
	public static final String WSQ_SQGAMESCORE = "wsqsqgamescore";
	/**
	 * 数钱游戏比赛成绩缓存key       
	 */
	public static final String WSQ_SQACTION = "wsqsqaction";
	
	/**
	 * 摇一摇游戏用户排名缓存
	 */
	public static final String WSQ_SQRANK= "wsqsqrank";
	
	/**
	 * 猜拳游戏人数缓存key
	 */
	public static final String WSQ_CQGAME = "wsqcqgame";
	/**
	 * 猜拳游戏临时用户缓存key
	 */
	public static final String WSQ_CQGAMELK = "wsqcqgamelk";
	/**
	 * 猜拳游戏临时用户缓存key
	 */
	public static final String WSQ_CQGAMEBIGSTATE = "wsqcqgamebigstate";
	/**
	 * 猜拳游戏用户状态缓存key
	 */
	public static final String WSQ_CQGAMEUSERSTATE = "wsqcqgameuserstate";
	
	/**
	 * 猜拳游戏比赛状态缓存key
	 */
	public static final String WSQ_CQGAMESTATE = "wsqcqgamestate";
	
	/**
	 * 猜拳游戏系统出拳缓存key
	 */
	public static final String WSQ_CQHSTATE = "wsqcqhstate";
	
	/**
	 * 猜拳游戏比赛成绩缓存key       
	 */
	public static final String WSQ_CQGAMESCORE = "wsqcqgamescore";
	/**
	 * 猜拳游戏比赛成绩缓存key       
	 */
	public static final String WSQ_CQACTION = "wsqcqaction";
	
	/**
	 * 猜拳游戏用户排名缓存
	 */
	public static final String WSQ_CQRANK= "wsqcqrank";
	
	/**
	 * 猜拳游戏剩余人数缓存
	 */
	public static final String WSQ_CQSURPLUSNUM= "wsqcqsurplusNum";
	
	/**
	 * 猜拳游戏淘汰人数缓存
	 */
	public static final String WSQ_CQELIMINATENUM= "wsqcqeliminateNum";
	
	/**
	 * 接受霸屏信息缓存key       
	 */
	public static final String WSQ_CONTENT = "wsqcontent";

	/**
	 * 游戏记录id缓存key
	 */
	public static final String GAME_RECORD_ID = "recordid";

	public static final String SUBSCRITE = "subscrite";

	/**
	 * 用户登陆返回微上墙user缓存key
	 */
	public static final String USER_INFO_ID = "userinfo";
	/**
	 * 视频路径转图片路径 参数
	 * /yeshenghuo/upload/201712/2a2a0b142f0c4553a36a0d77fa44e5b6.mp4 reture
	 * /yeshenghuo/upload/201712/2a2a0b142f0c4553a36a0d77fa44e5b6.jpg
	 */
	public static String videoToImg(String videoUrl) {
		String videoimg = null;
		try {
			if(videoUrl != null && videoimg.contains(".")){
				videoimg = videoUrl.substring(0, videoUrl.lastIndexOf(".")) + ".jpg";
				System.out.println(videoimg);
			}
		} catch (Exception e) {
			videoimg = null;
			e.printStackTrace();
		}
		return videoimg;
	}

	/** 
     * 去除字符串首尾出现的某个字符. 
     * @param source 源字符串. 
     * @param element 需要去除的字符. 
     * @return String. 
     */  
    public static String trimFirstAndLastChar(String source,char element){  
        boolean beginIndexFlag = true;  
        boolean endIndexFlag = true;  
        do{  
            int beginIndex = source.indexOf(element) == 0 ? 1 : 0;  
            int endIndex = source.lastIndexOf(element) + 1 == source.length() ? source.lastIndexOf(element) : source.length();  
            source = source.substring(beginIndex, endIndex);  
            beginIndexFlag = (source.indexOf(element) == 0);  
            endIndexFlag = (source.lastIndexOf(element) + 1 == source.length());  
        } while (beginIndexFlag || endIndexFlag);  
        return source;  
    } 
    public static void main(String[] args) {
    	System.out.println(trimFirstAndLastChar("opop,", ','));
    	String str="22222222";
    	int indexOf = str.indexOf(",,");
    	str=str.substring(0, indexOf)+str.substring(indexOf+1);
    	System.out.println(str);
	}
}
