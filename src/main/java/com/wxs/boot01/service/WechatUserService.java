package com.wxs.boot01.service;

import com.wxs.boot01.entity.WechatUser;

public interface WechatUserService {

    WechatUser selectByPrimaryKey(Integer id) throws Exception;

}
