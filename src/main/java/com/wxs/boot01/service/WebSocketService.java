package com.wxs.boot01.service;

import com.wxs.boot01.entity.WeishangqiangContent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface WebSocketService {

    List<Map<String,Object>> contentInfoList(Integer businessId) throws Exception;

    Integer insertSelective(WeishangqiangContent weishangqiangContent) throws Exception;

}
