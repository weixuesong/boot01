package com.wxs.boot01.service.impl;

import com.wxs.boot01.entity.WechatUser;
import com.wxs.boot01.entity.WeishangqiangContent;
import com.wxs.boot01.mapper.WechatUserMapper;
import com.wxs.boot01.mapper.WeishangqiangContentMapper;
import com.wxs.boot01.service.WebSocketService;
import com.wxs.boot01.service.WechatUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class WechatUserServiceImpl implements WechatUserService {

    @Resource
    private WechatUserMapper wechatUserMapper;

    @Override
    public WechatUser selectByPrimaryKey(Integer id) throws Exception {
        return wechatUserMapper.selectByPrimaryKey(id);
    }
}
