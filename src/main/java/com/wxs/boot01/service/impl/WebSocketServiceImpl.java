package com.wxs.boot01.service.impl;

import com.wxs.boot01.entity.WeishangqiangContent;
import com.wxs.boot01.mapper.WeishangqiangContentMapper;
import com.wxs.boot01.service.WebSocketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class WebSocketServiceImpl implements WebSocketService {

    @Resource
    private WeishangqiangContentMapper weishangqiangContentMapper;

    public List<Map<String,Object>> contentInfoList(Integer businessId) throws Exception {
        List<Map<String,Object>> list=weishangqiangContentMapper.queyContentInfo(businessId);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = list.get(i);
                    //map.put("createTime", new SimpleDateFormat("MM-dd HH:mm").format(map.get("create_time")));
                if (map.get("img_url") == null) {
                    map.put("img_url", "");
                } else {
                    map.put("img_url", String.valueOf(map.get("img_url")).split(","));
                }
                if (map.get("img_url_small") == null) {
                    map.put("img_url_small", "");
                } else {
                    map.put("img_url_small", String.valueOf(map.get("img_url_small")).split(","));
                }
            }
        }
        Collections.reverse(list);
        return list;
    }

    @Override
    public Integer insertSelective(WeishangqiangContent weishangqiangContent) throws Exception {
        return weishangqiangContentMapper.insertSelective(weishangqiangContent);
    }


}
