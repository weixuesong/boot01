package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.io.Serializable;

@Data
public class WeishangqiangVideo  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private Integer weishangqiangId;

    private String videoName;

    private String videoUrl;

}