package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;

@Data
public class Weishangqiang implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String pause;

    private String name;

    private String qrcode;

    private String location;

    private String title;

    private String bgUrl;

    private String stopWords;

    private String accessAccount;

    private Date createTime;

    private String bigScreenUrl;
    
    private String tanMuUrl;

    private String mobileScreenUrl;

    private String fontSize;

    private Byte available;

    private String bgVideo;

    private Long lastScreen;

    private Boolean screenCheck;

    private String fontColor;

    private String fontLight;

    private String contentBgOpacity;
    
    private String MobileURL;
    
    private Double rpLimitMoney;
    
    private Integer rpLimitTime;
    
    private Boolean friendCheck;
    
    private String userQrcode;
    
    private Boolean userQrcodeStatus;
    
    private String logoQrcode;

    private int business;
	
}