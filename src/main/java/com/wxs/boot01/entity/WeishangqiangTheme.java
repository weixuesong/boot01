package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
public class WeishangqiangTheme  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String themeName;

    private String themePic;

    private Boolean isOpen;

    //根据type 换颜色 前端需要
    private Integer type;
    
    private Integer switchTime;

    private Integer switchNum;

    private String leader;

    private String video;
    
    private Integer weishangqiangId;
    
    private MultipartFile file;
   
    private String Bfile;

    private String gnThemePic;

    private String gnVideo;

    public WeishangqiangTheme() {
		
	}

	public WeishangqiangTheme(Integer id, String themeName, String themePic, Boolean isOpen, Integer switchTime,
			Integer switchNum, String leader, String video, Integer weishangqiangId,String gnThemePic,String gnVideo) {
		this.id = id;
		this.themeName = themeName;
		this.themePic = themePic;
		this.isOpen = isOpen;
		this.switchTime = switchTime;
		this.switchNum = switchNum;
		this.leader = leader;
		this.video = video;
		this.weishangqiangId = weishangqiangId;
		this.gnThemePic=gnThemePic;
		this.gnVideo=gnVideo;
	}
}