package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;

@Data
public class BusinessWechatUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private Integer wechatUserId;
	
	private Integer businessId;
	
	private String role;

    private Integer ownScreenCount;

    private Integer ownScreenNum;

    private Integer ownScreenTime;
	
	private String notes;

	//禁言时长
	private String shutUpTime;
	//禁言到期时间
	private Date shutUpEndTime;
	
	private Date lastTime;

	private Integer songPushFlag;

	private Integer subscribeNew;
}
