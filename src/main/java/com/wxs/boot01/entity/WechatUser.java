package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Data
public class WechatUser implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String nickname;

    private String profile;
    
    private int subscribe; //订阅
    
    private String openId;
    
    private short sex;
    
    private String address;
    
    private String role;
    
    private int ownScreenCount;
    
    private Double balance;

	/*
         * 数据库中的等级
         */
    private Double level;
    /*
     * 用户排名
     */
    private int rank;
    
    private Integer bpTimes;
    /*
     * 扩充字段 	用户等级信息
     */
    private UserLevel userlevel;
    //被赞次数
  	private Integer praise;
  	//年龄	
  	private Integer age;
  	//最满意的部位
  	private String theMostSatisfyingPart;
  	//交友感言
  	private String datingSpeech;
    
    /*
     * 扩充字段 	座驾信息
     */
    private Integer carInfoId;

	/*
     * 扩充字段 	座驾图片路径
     */

	private String carUrl;
	/*
    * 扩充字段 	摇一摇游戏次数
    */
	private int shakeNumber;

	/*
	  vip
	 */
	private String lable;

	private String nameplate;

	private String vipHeadFrame;

	private String vipBadge;

	private String emoticon;

	private String admissionAnimation;

	private String pendant;

	private int userVipLevel;

	private String emoticonEndTime;

	private String vipEndTime;

	private int animationStatus;


	/*@Override
	public String toString() {
		return "WechatUser [id=" + id + ", nickname=" + nickname + ", profile=" + profile + ", sex=" + sex 
				+ ", userlevel=" + userlevel + ", praise=" + praise + ", age=" + age
				+ ", theMostSatisfyingPart=" + theMostSatisfyingPart + ", datingSpeech=" + datingSpeech + "]";
	}
*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WechatUser other = (WechatUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}