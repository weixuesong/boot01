package com.wxs.boot01.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserLevel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * 当前等级 头衔
	 */
	private String level;  
	/*
	 * 当前等级(lv0)
	 */
	private String lv;  
	/*
	 * 当前头像
	 */
	private String src;
	/*
	 * 当前魅力值
	 */
	private int currentGlamour;
	/*
	 * 升级所需魅力值
	 */
	private int needGlamour;
	
}
