package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.io.Serializable;
import java.util.Date;

@Data
public class WeishangqiangContent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer wechatUserId;

	private Integer weishangqiangId;

	private Integer type;

	private Integer ownScreenId;

	private Integer dashangId;

	private Integer dashangTarId;

	private String imgUrl;

	private String imgUrlSmall;

	private String content;

	private Date createTime;

	private Integer themeId;

	private String videoUrl;

	private Integer toWechatUserId;

	private Boolean admin;

	private String orderId;

	private Double level;
	/** 扩展临时字段 by wangyadong **/
	private String profile;
	private String nickname;
	private Short sex;
	private Short duration;
	private String dashangName;
	private String dashangImg;
	private String dashangTarName;
	private String toWechatUserName;
	private String themePic;
	private String themeName;
	private String needScreenUserNickname;
	private String transformTime;
	private int subscribe;
	private String seatNum;
	private int screenNum;
	//座驾图片路径
	private String carUrl;

}