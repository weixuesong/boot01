package com.wxs.boot01.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class Business implements Serializable {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String logo;

    private String qrcode;

    private String name;

    private String addrress;

    private String tel;

    private String summary;

    private String bank;

    private String bankLocation;

    private String bankPossessor;

    private String bankAccount;
    
    private BigDecimal ratio;

    private String authcode;

    private String notes;
    
    private Integer showed;
    /**
     * 酒吧余额
     */
    private BigDecimal balance;
    /**
     * 营收总额
     */
    private BigDecimal revenues;
    /**
     *实际余额
     */
    private BigDecimal actualBalance;
    
    private String shortUrl;

	private int preferenceState;

    private BigDecimal turnover;
    
    private Date lastTime;
    
    private String lastTimeTwo;

	@Override
	public String toString() {
		return "Business [id=" + id + ", logo=" + logo + ", qrcode=" + qrcode + ", name=" + name + ", addrress="
				+ addrress + ", tel=" + tel + ", summary=" + summary + ", bank=" + bank + ", bankLocation="
				+ bankLocation + ", bankPossessor=" + bankPossessor + ", bankAccount=" + bankAccount + ", balance="
				+ balance + ", ratio=" + ratio + ", authcode=" + authcode + ", notes=" + notes + ", showed=" + showed
				+ ", revenues=" + revenues + ", shortUrl=" + shortUrl + ", actualBalance=" + actualBalance
				+ ", turnover=" + turnover + "]";
	}
	
}